#. extracted from content/editions/workstation/home.yml
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-26 13:33-0700\n"
"PO-Revision-Date: 2023-01-08 23:20+0000\n"
"Last-Translator: Josef Hruska <hrusjos@gmail.com>\n"
"Language-Team: Czech <https://translate.fedoraproject.org/projects/"
"fedora-websites-3-0/editionsworkstationhome/cs/>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Weblate 4.15\n"

#: path
msgid "index"
msgstr ""

#: title
msgid "The leading Linux desktop"
msgstr "Linuxový desktop na špici"

#: description
msgid ""
"A beautiful, high-quality desktop, built on the latest open source "
"technology. Trusted, powerful and easy.\n"
msgstr ""
"Krásný, vysoce kvalitní desktop, postavený na nejnovější open-source "
"technologii. Spolehlivý, výkonný a snadný.\n"

#: header_images-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_framework.png"
msgstr ""

#: header_images-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_background.jpg"
msgstr ""

#: links-%3E[0]-%3Etext
msgid "Download now"
msgstr "Hned stáhnout"

#: links-%3E[0]-%3Eurl
#, read-only
msgid "/workstation/download"
msgstr ""

#: links-%3E[1]-%3Etext
msgid "Reviewed By TechHut"
msgstr "Recenze od TechHut"

#: links-%3E[1]-%3Eurl
#, read-only
msgid "https://www.youtube.com/watch?v=54aoIGKEMnk"
msgstr ""

#: sections-%3E[0]-%3EsectionTitle
msgid "Why Fedora Workstation?"
msgstr "Proč Fedoru Workstation?"

#: sections-%3E[0]-%3Econtent-%3E[0]-%3Etitle
msgid "Reliable"
msgstr "Spolehlivá"

#: sections-%3E[0]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"Each version is updated for 18 months, and upgrades between versions are "
"quick and easy."
msgstr ""
"Každá verze je aktualizována po 13 měsíců a přechod mezi verzemi je rychlý a "
"snadný."

#: sections-%3E[0]-%3Econtent-%3E[1]-%3Etitle
msgid "Free & private"
msgstr "Svobodná & ctí soukromí"

#: sections-%3E[0]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"With Fedora, your desktop is your own. It's free, there are no ads, and your "
"data belongs to you."
msgstr ""
"S Fedorou je váš desktop zkrátka jen váš. Je svobodný, bez žádných reklam a "
"vaše data patří jen a pouze vám."

#: sections-%3E[0]-%3Econtent-%3E[2]-%3Etitle
msgid "Beautiful"
msgstr "Krásná"

#: sections-%3E[0]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"Workstation is carefully curated to deliver a high-quality experience. The "
"desktop is clean and uncluttered."
msgstr ""
"Workstation je připravována s největší péčí, aby poskytovala vysoce kvalitní "
"zážitek. Prostředí pracovní plochy je čisté a přehledné."

#: sections-%3E[0]-%3Econtent-%3E[3]-%3Etitle
msgid "Trusted"
msgstr "Důvěryhodná"

#: sections-%3E[0]-%3Econtent-%3E[3]-%3Edescription
msgid ""
"Developed in partnership with upstream projects. Rigorously tested. Backed "
"by Red Hat."
msgstr ""
"Vyvíjena v partnerství s dodavatelskými (upstream) projekty. Pečlivě "
"testována. Podporována společností Red Hat."

#: sections-%3E[0]-%3Econtent-%3E[4]-%3Etitle
msgid "Leading technology"
msgstr "Technologie na špici"

#: sections-%3E[0]-%3Econtent-%3E[4]-%3Edescription
msgid ""
"Built on the latest technologies and enhancements that open source has to "
"offer."
msgstr ""
"Postavena na nejnovějších technologiích a pokroku, které open source nabízí."

#: sections-%3E[0]-%3Econtent-%3E[5]-%3Etitle
msgid "Makes the most of your device"
msgstr "Vytáhne to nejlepší z vašeho zařízení"

#: sections-%3E[0]-%3Econtent-%3E[5]-%3Edescription
msgid ""
"Fedora works with hardware vendors to make excellent hardware support across "
"a range of devices."
msgstr ""
"Fedora spolupracuje s výrobci hardwaru, aby obsahovala výtečnou podporu pro "
"hardware na řadě zařízení."

#: sections-%3E[1]-%3EsectionTitle
msgid "Features for everyone"
msgstr "Vlastnosti pro každého"

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Etitle
msgid "Fantastic app"
msgstr "Fantastické aplikace"

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"Browse a great collection of apps, covering everything you might need. "
"Includes exciting new open source apps as well as familiar apps from other "
"platforms."
msgstr ""
"Projděte si skvělou sbírku aplikací pokrývající vše co můžete potřebovat. "
"Obsahuje vzrušující nové open-source aplikace a rovněž známé aplikace z "
"ostatních platforem."

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_software_center.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Etitle
msgid "Workstation speaks your language"
msgstr "Workstation hovoří vaším jazykem"

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"Thanks to a global community of translators, Workstation is available in "
"many languages."
msgstr ""
"Díky celosvětové komunitě překladatelů, Workstation je k dispozici v mnoha "
"jazycích."

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_languages.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Etitle
msgid "Turn the lights down low"
msgstr "Ztlumte osvětlení"

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"Flip a switch to turn on dark mode and give your eyes a break from the day. "
"Use night light to reduce screen glare and help with sleep."
msgstr ""
"Přepněte přepínač pro zapnutí tmavého režimu a ulevte svým očím. Použijte "
"noční světlo pro snížení jasu a snadnější usínání."

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_nightlight.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[3]-%3Etitle
msgid "Calendar integration"
msgstr "Integrace s kalendářem"

#: sections-%3E[1]-%3Econtent-%3E[3]-%3Edescription
msgid "Bring your online calendar to the desktop with online accounts"
msgstr "Přeneste si on-line kalendář na pracovní plochu díky on-line účtům"

#: sections-%3E[1]-%3Econtent-%3E[3]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_calendar.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[4]-%3Etitle
msgid "Helpful out of the box"
msgstr "Připravena okamžitě pomoci"

#: sections-%3E[1]-%3Econtent-%3E[4]-%3Edescription
msgid ""
"Fedora Workstation includes a great set of utilities, like Clocks, Weather "
"and Maps."
msgstr ""
"Fedora Workstation obsahuje velkou sadu pomocných programů, jako např. "
"hodiny, počasí nebo mapy."

#: sections-%3E[1]-%3Econtent-%3E[4]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_outofthebox.png"
msgstr ""

#: sections-%3E[2]-%3EsectionTitle
msgid "Great for developers"
msgstr "Výtečná pro vývojáře"

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Etitle
msgid "Virtualization made easy"
msgstr "Virtualizace jednoduše"

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Edescription
msgid "Boxes takes the guesswork out of using virtual machines."
msgstr ""
"Boxy neponechávají žádné otázky náhodě při používání virtuálních strojů."

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/dev1.png"
msgstr ""

#: sections-%3E[2]-%3Econtent-%3E[1]-%3Etitle
msgid "Productivity-boosting desktop features"
msgstr "Vlastnosti prostředí pracovní plochy, které zvyšují produktivitu"

#: sections-%3E[2]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"Use performance mode to boost hardware speed when you need it. Turn off "
"notifications with Do Not Disturb. Press the Super key and just type to "
"search for what you need."
msgstr ""
"Použijte výkonný režim, abyste navýšili rychlost hardwaru, když ji "
"potřebujete. Vypněte oznámení možností Nevyrušovat. Stiskněte klávesu Super "
"a jen napište co potřebujete nalézt."

#: sections-%3E[2]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/dev2.png"
msgstr ""

#: sections-%3E[2]-%3Econtent-%3E[2]-%3Etitle
msgid "Containers ready to go"
msgstr "Připravena na kontejnery"

#: sections-%3E[2]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"Use the latest container tools from the Red Hat ecosystem. Not setup "
"required. Access the Red Hat container registry."
msgstr ""
"Používejte nejnovější kontejnerové nástroje z ekosystému Red Hat. Žádné "
"nastavování není třeba. Vstupte do registru kontejnerů Red Hat."

#: sections-%3E[2]-%3Econtent-%3E[2]-%3Eimage
#, read-only
msgid "public/assets/images/dev3.png"
msgstr ""

#: sections-%3E[2]-%3Econtent-%3E[3]-%3Etitle
msgid "All the tools"
msgstr "Všechny nástroje"

#: sections-%3E[2]-%3Econtent-%3E[3]-%3Edescription
msgid ""
"All the tools you might need, easy to install with a single command. Tools "
"supplied with high quality, by the people who make them."
msgstr ""
"Všechny nástroje, které můžete potřebovat, jednoduše nainstalovatelné "
"jediným příkazem. Nástroje dodávané s vysokou kvalitou, lidmi, kteří je "
"tvoří."

#: sections-%3E[2]-%3Econtent-%3E[3]-%3Eimage
#, read-only
msgid "public/assets/images/dev4.png"
msgstr ""

#: sections-%3E[3]-%3EsectionTitle
msgid "Get started developing with Fedora Workstation"
msgstr "Začněte vyvíjet s Fedora Workstation"

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"The Fedora Developer portal has guides on developing and deploying "
"applications using Fedora workstation."
msgstr ""
"Portál Fedory pro vývojáře obsahuje návody na vývoj a zprovoznění aplikací "
"při používání Fedory Workstation."

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Eurl
#, read-only
msgctxt "sections->[3]->content->[0]->url"
"sections->[3]->content->[0]->url"
msgid "https://fedoradeveloper.com"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/fedora-devs.png"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"DNF is the Fedora package manager that enables you to install, update and "
"manage OS components."
msgstr ""
"DNF je ve Fedoře správce balíčků, který vám umožní nainstalovat, "
"aktualizovat a spravovat součásti OS."

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Eurl
#, read-only
msgctxt "sections->[3]->content->[1]->url"
msgid "https://fedoradeveloper.com"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/logo-dnf.png"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"Podman is a daemonless container engine for creating and managing containers "
"and container images."
msgstr ""
"Podman je daemonless kontejnerový stroj pro tvorbu a správu kontejnerů a "
"kontejnerových obrazů."

#: sections-%3E[3]-%3Econtent-%3E[2]-%3Eurl
#, read-only
msgctxt "sections->[3]->content->[2]->url"
msgid "https://fedoradeveloper.com"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[2]-%3Eimage
#, read-only
msgid "public/assets/images/podman.svg"
msgstr ""

#: sections-%3E[4]-%3EsectionTitle
msgid "Built by you"
msgstr "Vyvíjena vámi"

#: sections-%3E[4]-%3Eimages
msgid "public/assets/images/iot_flock_background.jpg"
msgstr ""

#: sections-%3E[4]-%3EsectionDescription
msgid ""
"The Fedora Project envisions a world where everyone can benefit from free "
"and open source software built by inclusive, welcoming, and open-minded "
"communities.\n"
"Fedora Workstation is created by a team in the Fedora community called the **"
"Workstation Working Group**. It is comprised of official members who have "
"decision making powers, as well as other contributors. [Learn more onthe "
"Workstation Working Group Website](https://docs.fedoraproject.org/en-US/"
"workstation-working-group/)"
msgstr ""
"Projekt Fedora si představuje svět, kde každý může mít prospěch ze "
"svobodného a open-source softwaru, který je vyvíjen inkluzivními, vlídnými a "
"myšlenkově otevřenými komunitami.\n"
"Fedora Workstation je vytvářena týmem z komunity Fedory s názvem **Pracovní "
"skupina Workstation**. Skládá se z oficiálních členů, kteří mají rozhodovací "
"pravomoc, a dalších přispěvatelů. [Zjistěte si více na webových stránkách "
"Pracovní skupiny Workstation](https://docs.fedoraproject.org/en-US/"
"workstation-working-group/)"

#: sections-%3E[4]-%3Econtent-%3E[0]-%3Etitle
msgid "Report & discuss issues"
msgstr "Podávejte & diskutujte hlášení"

#: sections-%3E[4]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"You can view, file, and discuss Fedora Workstation issues on [the Fedora "
"Workstation issue tracker]()."
msgstr ""
"Můžete si prohlédnout, podat a diskutovat hlášení o Fedoře Workstation na ["
"monitoringu hlášení Fedory Workstation]()."

#: sections-%3E[4]-%3Econtent-%3E[1]-%3Etitle
msgid "Chat with the team"
msgstr "Chatujte s týmem"

#: sections-%3E[4]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"Visit **#fedora-workstation** on [irc.libera.chat]() and on **#workstation** "
"[on the Fedora Matrix Chat server]()."
msgstr ""
"Navštivte kanál **#fedora-workstation** na [irc.libera.chat]() a místnost "
"**#workstation** na [Matrix chat serveru Fedory]()."

#: sections-%3E[4]-%3Econtent-%3E[2]-%3Etitle
msgid "Join the mailing list"
msgstr "Přihlaste se do elektronické poštovní konference"

#: sections-%3E[4]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"Sign up at [desktop@lists.fedoraproject.org]() to receive meeting agendas "
"and minute ([view archives]().)"
msgstr ""
"Zaregistrujte se na [desktop@lists.fedoraproject.org]() a dostávejte "
"programy jednání a zápisy ([zobrazit archiv]().)"

#: sections-%3E[4]-%3Econtent-%3E[3]-%3Etitle
msgid "Attend a meeting"
msgstr "Zúčastněte se jednání"

#: sections-%3E[4]-%3Econtent-%3E[3]-%3Edescription
msgid ""
"Open to all current and potential contributors on **Tuesdays, 10:00 AM US "
"Eastern** on [Bluejeans]()."
msgstr ""
"Otevřené všem současným i potenciálním přispěvatelů v **Úterý, 10:00 US "
"Eastern** na [Bluejeans]()."
